'use strict';

/*
  * The tests in this file read and write to an external MongoDB database
  * To specify the MongoDB URI, set an environment variable called 'TEST_DB'
  * equal to the desired URI to a '.env' file in the root directory 
*/

const { fork } = require('fluture');
const mongoose = require('mongoose');
const path = require('path');
const Contest = require('../../src/core/db/models/Contest');
const queryCachedContest = require('../../src/core/main/augmentResponseBody/retrieveContest/queryCachedContest');
const testContest = require('../mockData/testContest2');
const mockResponse = require('../mockData/mockResponse');
const upsertContests = require('../../src/core/main/upsertContests');
const contestQueryFilter = require('../../src/core/db/queryFilters/contestQueryFilter');
require('dotenv').config({ path: path.resolve(__dirname, '../../.env') });

describe('database integration', () => {
  beforeAll(async () => {
    await mongoose.connect(
      process.env.TEST_DB,
      {
        bufferMaxEntries: 0,
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true
      });
    await Contest.deleteMany({});
    await new Contest(testContest).save();
  });

  xtest('queryCachedContest resolves with the existing document if given a duplicate contest', done => {
    expect.assertions(2);
    const onFailure = jest.fn(() => { done(); });
    const onSuccess = res => {
      expect(onFailure).not.toBeCalled();
      expect(res).toEqual({
        candidates: expect.any(Array),
        district: expect.any(Object),
        office: expect.any(String)
      });
      done();
    };

    queryCachedContest(testContest).pipe(fork(onFailure)(onSuccess));
  });

  xtest('upsertContests upserts all augmented contests and resolves with an array given a ballot', async () => {
    expect.assertions(2);
    const shouldBeArray = upsertContests(mockResponse);
    const retrievedContest = await Contest
      .findOne(contestQueryFilter(mockResponse.contests.augmented[0]))
      .select('-_id -__v -expireAt')
      .orFail()
      .exec();

    expect(Array.isArray(shouldBeArray)).toBeTruthy();
    expect(retrievedContest.toObject().candidates.length).toBe(3);
  });

  afterAll(async () => {
    try {
      await mongoose.connection.db.dropDatabase();
      await mongoose.connection.close();
    } catch (e) {
      console.error(e);
    }
  });
});

