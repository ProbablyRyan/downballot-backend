module.exports = {
    "candidates": [
      {
        "name": {
          "full": "John Doe",
          "first": "John",
          "last": "Doe"
        }
      },
      {
        "name": {
          "full": 'Smitty Werbenjagermanjensen',
          "first": 'Smitty',
          "last": 'Werbenjagermanjensen'
        }
      }
    ],
    "district": {
      "name": 'Fake District',
    },
    "expireAt": new Date().getTime() + 60000, // One minute
    "office": 'Fake Office'
  };
  