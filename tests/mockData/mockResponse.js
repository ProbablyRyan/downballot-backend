module.exports = {
  "election": {
      "id": "2000",
      "name": "VIP Test Election",
      "electionDay": "2021-06-06",
      "ocdDivisionId": "ocd-division/country:us"
    },
    "contests": {
      "augmented": [
        {
          "office": "United States Senate",
          "district": {
            "name": "Kansas",
            "scope": "statewide",
            "id": "ocd-division/country:us/state:ks"
          },
          "candidates": [
            {
              "name": {
                "first": "Greg",
                "last": "Orman",
                "full": "Greg Orman"
              },
              "email": "KeenforKansas@aol.com",
              "phone": "555-555-5555",
              "party": "Independent",
              "candidateUrl": "http://www.ormanforsenate.com/",
              "channels": [
                {
                  "type": "Facebook",
                  "id": "https://www.facebook.com/ormanforsenate"
                },
                {
                  "type": "Twitter",
                  "id": "https://twitter.com/OrmanForSenate"
                },
                {
                  "type": "YouTube",
                  "id": "https://www.youtube.com/channel/UCjh9PJawhbnEB07_xECtAyA"
                }
              ],
              "votes": {
                "list": [
                  {
                    "title": "National Defense Authorization Act for Fiscal Year 2021",
                    "vote": "Nay"
                  },
                  {
                    "title": "Great American Outdoors Act",
                    "vote": "Yay"
                  },
                  {
                    "title": "Nomination of John Ratcliffe as Director of National Intelligence",
                    "vote": "Abstained"
                  },
                  {
                    "title": "USA FREEDOM Reauthorization Act of 2020",
                    "vote": "Abstained"
                  },
                  {
                    "title": "Uyghur Human Rights Policy Act of 2020",
                    "vote": "Co-sponsor"
                  }
                ],
                "source": "https://justfacts.votesmart.org/candidate/key-votes/27110/bernie-sanders"
              },
              "contributors": {
                "list": [
                  {
                    "name": "Alphabet Inc",
                    "amount": "$979,056.00"
                  },
                  {
                    "name": "University of California",
                    "amount": "$918,016.00"
                  },
                  {
                    "name": "Amazon.com",
                    "amount": "$788,508.00"
                  },
                  {
                    "name": "US Postal Service",
                    "amount": "$442,786.00"
                  },
                  {
                    "name": "Microsoft Corp",
                    "amount": "$430,058.00"
                  }
                ],
                "source": "https://justfacts.votesmart.org/candidate/campaign-finance/27110/bernie-sanders"
              }
            },
            {
              "name": {
                "first": "Pat",
                "last": "Roberts",
                "full": "Pat Roberts"
              },
              "email": "KeenforKansas@aol.com",
              "phone": "555-555-5555",
              "party": "Republican",
              "candidateUrl": "http://www.robertsforsenate.com/",
              "channels": [
                {
                  "type": "Facebook",
                  "id": "https://www.facebook.com/RobertsForSenate"
                },
                {
                  "type": "Twitter",
                  "id": "https://twitter.com/PatRoberts2014"
                },
                {
                  "type": "YouTube",
                  "id": "https://www.youtube.com/channel/UCp7_3Je4VtJODR1HL2xXhGw"
                }
              ],
              "votes": {
                "list": [
                  {
                    "title": "National Defense Authorization Act for Fiscal Year 2021",
                    "vote": "Nay"
                  },
                  {
                    "title": "Great American Outdoors Act",
                    "vote": "Yay"
                  },
                  {
                    "title": "Nomination of John Ratcliffe as Director of National Intelligence",
                    "vote": "Abstained"
                  },
                  {
                    "title": "USA FREEDOM Reauthorization Act of 2020",
                    "vote": "Abstained"
                  },
                  {
                    "title": "Uyghur Human Rights Policy Act of 2020",
                    "vote": "Co-sponsor"
                  }
                ],
                "source": "https://justfacts.votesmart.org/candidate/key-votes/27110/bernie-sanders"
              },
              "contributors": {
                "list": [
                  {
                    "name": "Alphabet Inc",
                    "amount": "$979,056.00"
                  },
                  {
                    "name": "University of California",
                    "amount": "$918,016.00"
                  },
                  {
                    "name": "Amazon.com",
                    "amount": "$788,508.00"
                  },
                  {
                    "name": "US Postal Service",
                    "amount": "$442,786.00"
                  },
                  {
                    "name": "Microsoft Corp",
                    "amount": "$430,058.00"
                  }
                ],
                "source": "https://justfacts.votesmart.org/candidate/campaign-finance/27110/bernie-sanders"
              }
            },
            {
              "name": {
                "first": "Randall",
                "last": "Batson",
                "full": "Randall Batson"
              },
              "email": "KeenforKansas@aol.com",
              "phone": "555-555-5555",
              "party": "Libertarian",
              "candidateUrl": "http://batson4senate.weebly.com/",
              "channels": [
                {
                  "type": "Facebook",
                  "id": "https://www.facebook.com/batsonforsenate"
                }
              ],
              "votes": {
                "list": [
                  {
                    "title": "National Defense Authorization Act for Fiscal Year 2021",
                    "vote": "Nay"
                  },
                  {
                    "title": "Great American Outdoors Act",
                    "vote": "Yay"
                  },
                  {
                    "title": "Nomination of John Ratcliffe as Director of National Intelligence",
                    "vote": "Abstained"
                  },
                  {
                    "title": "USA FREEDOM Reauthorization Act of 2020",
                    "vote": "Abstained"
                  },
                  {
                    "title": "Uyghur Human Rights Policy Act of 2020",
                    "vote": "Co-sponsor"
                  }
                ],
                "source": "https://justfacts.votesmart.org/candidate/key-votes/27110/bernie-sanders"
              },
              "contributors": {
                "list": [
                  {
                    "name": "Alphabet Inc",
                    "amount": "$979,056.00"
                  },
                  {
                    "name": "University of California",
                    "amount": "$918,016.00"
                  },
                  {
                    "name": "Amazon.com",
                    "amount": "$788,508.00"
                  },
                  {
                    "name": "US Postal Service",
                    "amount": "$442,786.00"
                  },
                  {
                    "name": "Microsoft Corp",
                    "amount": "$430,058.00"
                  }
                ],
                "source": "https://justfacts.votesmart.org/candidate/campaign-finance/27110/bernie-sanders"
              }
            }
          ]
        },
        {
          "office": "United States Senate",
          "district": {
            "name": "Kansas",
            "scope": "statewide",
            "id": "ocd-division/country:us/state:ks"
          },
          "candidates": [
            {
              "name": {
                "first": "Greg",
                "last": "Orman",
                "full": "Greg Orman"
              },
              "email": "KeenforKansas@aol.com",
              "phone": "555-555-5555",
              "party": "Independent",
              "candidateUrl": "http://www.ormanforsenate.com/",
              "channels": [],
              "votes": {
                "list": []
              },
              "contributors": {
                "list": [
                  {
                    "name": "Alphabet Inc",
                    "amount": "$979,056.00"
                  },
                  {
                    "name": "University of California",
                    "amount": "$918,016.00"
                  },
                  {
                    "name": "Amazon.com",
                    "amount": "$788,508.00"
                  },
                  {
                    "name": "US Postal Service",
                    "amount": "$442,786.00"
                  },
                  {
                    "name": "Microsoft Corp",
                    "amount": "$430,058.00"
                  }
                ],
                "source": "https://justfacts.votesmart.org/candidate/campaign-finance/27110/bernie-sanders"
              }
            },
            {
              "name": {
                "first": "Pat",
                "last": "Roberts",
                "full": "Pat Roberts"
              },
              "party": "Republican",
              "email": undefined,
              "phone": undefined,
              "candidateUrl": "http://www.robertsforsenate.com/",
              "channels": [],
              "votes": {
                "list": []
              },
              "contributors": {
                "list": [
                  {
                    "name": "Alphabet Inc",
                    "amount": "$979,056.00"
                  },
                  {
                    "name": "University of California",
                    "amount": "$918,016.00"
                  },
                  {
                    "name": "Amazon.com",
                    "amount": "$788,508.00"
                  },
                  {
                    "name": "US Postal Service",
                    "amount": "$442,786.00"
                  },
                  {
                    "name": "Microsoft Corp",
                    "amount": "$430,058.00"
                  }
                ],
                "source": "https://justfacts.votesmart.org/candidate/campaign-finance/27110/bernie-sanders"
              }
            },
            {
              "name": {
                "first": "Randall",
                "last": "Batson",
                "full": "Randall Batson"
              },
              "party": "Libertarian",
              "email": undefined,
              "phone": undefined,
              "candidateUrl": "http://batson4senate.weebly.com/",
              "channels": [],
              "votes": {
                "list": []
              },
              "contributors": {
                "list": []
              }
            }
          ]
        }
      ],
      "basic": [
        {
          "office": "Sheriff",
          "district": {
            "name": "Sheriff",
            "scope": "countywide",
            "id": "5"
          }
        }
      ],
      "referenda": [
        {
          "type": "Referendum",
          "district": {
            "name": "Kansas",
            "scope": "statewide",
            "id": "ocd-division/country:us/state:ks"
          },
          "referendumTitle": "CONSTITUTIONAL AMENDMENT - VOTE YES OR NO",
          "referendumSubtitle": "The constitution currently prohibits the operation of lotteries except for specifically authorized lotteries. A raffle is a lottery and is illegal under current law. A vote for this proposition would permit the legislature to authorize charitable raffles operated or conducted by religious, charitable, fraternal, educational and veterans nonprofit organizations subject to the limitations listed. Nonprofit organizations would be prohibited from contracting with a professional lottery vendor to manage, operate or conduct a charitable raffle. A vote against this proposition would continue the current prohibition against all raffles.",
          "referendumUrl": "http://www.kslegislature.org/li/b2013_14/measures/documents/scr1618_00_0000.pdf",
          "sources": [
            {
              "name": "Ballot Information Project",
              "official": false
            }
          ]
        },
        {
          "type": "Referendum",
          "district": {
            "name": "Kansas",
            "scope": "statewide",
            "id": "ocd-division/country:us/state:ks"
          },
          "referendumTitle": "CONSTITUTIONAL AMENDMENT - VOTE YES OR NO",
          "referendumSubtitle": "The constitution currently prohibits the operation of lotteries except for specifically authorized lotteries. A raffle is a lottery and is illegal under current law. A vote for this proposition would permit the legislature to authorize charitable raffles operated or conducted by religious, charitable, fraternal, educational and veterans nonprofit organizations subject to the limitations listed. Nonprofit organizations would be prohibited from contracting with a professional lottery vendor to manage, operate or conduct a charitable raffle. A vote against this proposition would continue the current prohibition against all raffles.",
          "referendumUrl": "http://www.kslegislature.org/li/b2013_14/measures/documents/scr1618_00_0000.pdf",
          "sources": [
            {
              "name": "Ballot Information Project",
              "official": false
            }
          ]
        }
      ]
    }
}
