module.exports = {
  status: 200,
  data: {
    "candidateList": {
      "generalInfo": {
        "title": "Project Vote Smart - Search Candidates",
        "linkBack": "http://votesmart.org/"
      },
      "candidate": [
        {
          "candidateId": "186877",
          "firstName": "Amanda",
          "nickName": "Jennings",
          "middleName": "",
          "preferredName": "Amanda",
          "lastName": "Smith",
          "suffix": "",
          "title": "",
          "ballotName": "Amanda 'Jennings' Smith"
        },
        {
          "candidateId": "186811",
          "firstName": "Antonio",
          "nickName": "",
          "middleName": "T.",
          "preferredName": "Antonio",
          "lastName": "Smith",
          "suffix": "Jr.",
          "title": "",
          "ballotName": "Antonio T. Smith Jr."
        },
        {
          "candidateId": "186569",
          "firstName": "Carlton",
          "nickName": "",
          "middleName": "E.",
          "preferredName": "Carlton",
          "lastName": "Smith",
          "suffix": "",
          "title": "",
          "ballotName": "Carlton E. Smith"
        },
        {
          "candidateId": "36485",
          "firstName": "Charles",
          "nickName": "",
          "middleName": "Ulysses",
          "preferredName": "Charles",
          "lastName": "Smith",
          "suffix": "",
          "title": "",
          "ballotName": "Charles U. Smith"
        }
      ]
    }
  }  
}