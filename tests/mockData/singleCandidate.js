module.exports = {
  status: 200,
  data: {
    "candidateList": {
      "generalInfo": {
        "title": "Project Vote Smart - Search Candidates",
        "linkBack": "http://votesmart.org/"
      },
      "candidate": {
        "candidateId": "15723",
        "firstName": "Donald",
        "nickName": "",
        "middleName": "J.",
        "preferredName": "Donald",
        "lastName": "Trump",
        "suffix": "",
        "title": "President",
        "ballotName": "Donald J. Trump"
      }
    }
  }
};
