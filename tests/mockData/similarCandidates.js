module.exports = module.exports = {
    status: 200,
    data: {
      "candidateList": {
        "generalInfo": {
          "title": "Project Vote Smart - Search Candidates",
          "linkBack": "http://votesmart.org/"
        },
        "candidate": [
          {
            "candidateId": "1234",
            "firstName": "John",
            "nickName": "",
            "middleName": "",
            "preferredName": "John",
            "lastName": "Doe",
            "suffix": "",
            "title": "",
            "ballotName": "John Doe"
          },
          {
            "candidateId": "4567",
            "firstName": "John",
            "nickName": "",
            "middleName": "",
            "preferredName": "John",
            "lastName": "Doe",
            "suffix": "",
            "title": "",
            "ballotName": "John Doe"
          }
        ]
      }
    }  
  };