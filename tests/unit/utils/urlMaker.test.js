'use strict';

const urlMaker = require('../../../src/core/utils/urlMaker');

describe('urlMaker', () => {
  it('returns a curried function which returns a string', () => {
    const fnOne = urlMaker();
    const fnTwo = fnOne();

    expect(typeof fnOne).toBe('function');
    expect(typeof fnTwo).toBe('string');
  });

  it('produces a url in nest file format of given a path as an array', () => {
    const makeUrl = urlMaker('www.site.com');
    expect(makeUrl(['some', 'path'])).toBe('www.site.com/some/path');
  });

  it('produces a url in query string format given query parameters as an object', () => {
    const makeUrl = urlMaker('www.site.com/query?');
    expect(makeUrl({ keyOne: 'valueOne', keyTwo: 'valueTwo' })).toBe('www.site.com/query?keyOne=valueOne&keyTwo=valueTwo&');
  });
});