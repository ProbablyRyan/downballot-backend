'use strict';

const Response = require('../../../src/core/utils/Response');

describe('Response', () => {
  it('creates new objects with a shared prototype', () => {
    const obj1 = Response({});
    const obj2 = Response({});
  
    expect(obj1 === obj2).toBeFalsy();
    expect(obj1.prototype === obj2.prototype).toBeTruthy();
  });

  it("returns objects that have a setBody method that sets the body property to it's input if 200 <= this.statusCode <= 299", () => {
    const obj1 = Response({ statusCode: 200 });
    const obj2 = Response({ statusCode: 404 });

    obj1.setBody({ someProperty: true });
    obj2.setBody({ someProperty: true });

    expect(obj1.body.someProperty).toBeTruthy();
    expect(obj2.body.someProperty).toBeFalsy();
  });

  it('returns objects that have a getBody method that returns a copy of this.body', () => {
    const obj = Response({
      statusCode: 200,
      body: {
        someProperty: true
      }
    });
    const copy = obj.getBody();

    expect(copy).not.toBe(obj.body);
    expect(copy).toEqual(obj.body);
  });

  it("returns objects that have a bodyAsJSON method that returns a copy of itself but with it's body property stringified", () => {
    const obj = Response({
      statusCode: 200,
      body: {
        someProperty: true
      }
    });
    const asJSON = obj.bodyAsJSON();

    expect(asJSON).not.toBe(obj);
    expect(asJSON).toMatchObject({
      ...obj,
      body: JSON.stringify(obj.body)
    });
  });
});