'use strict';

const doAsyncIO = require('../../../src/core/utils/doAsyncIO');

describe('doAsyncIO', () => {
  it('defaults to an identity fn as handler for both fulfilled and rejected promises', async () => {
    expect.assertions(2)
    const resolved = await doAsyncIO()((str) => Promise.resolve(str))('identity');
    const rejected = await doAsyncIO()((str) => Promise.reject(str))('identity');

    expect(resolved).toEqual('identity');
    expect(rejected).toEqual('identity');
  });

  it('calls the passed promise returning function with the passed resolution handler', async () => {
    expect.assertions(2);
    const promiseFn = jest.fn().mockImplementation(() => Promise.resolve('passed'));
    const resHandler = jest.fn();

    await doAsyncIO(resHandler)(promiseFn)();

    expect(promiseFn).toHaveBeenCalled();
    expect(resHandler).toBeCalledWith('passed');
  });

  it('calls the passed promise returning function with the passed rejection handler', async () => {
    expect.assertions(2);
    const promiseFn = jest.fn().mockImplementation(() => Promise.reject('passed'));
    const rejHandler = jest.fn();

    await doAsyncIO(null, rejHandler)(promiseFn)();

    expect(promiseFn).toHaveBeenCalled();
    expect(rejHandler).toBeCalledWith('passed');
  });

  it('calls the passed promise returning funcion with the passed arguments', async () => {
    expect.assertions(1);
    const promiseFn = jest.fn().mockImplementation(() => Promise.resolve());

    await doAsyncIO()(promiseFn)('args');

    expect(promiseFn).toBeCalledWith('args');
  });
});