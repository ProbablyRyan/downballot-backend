'use strict';

const { isFuture, resolve, fork } = require('fluture');
const initResponseFromAddress = require('../../../src/core/main/initResponseFromAddress');
const makeVoterInfoQuery = require('../../../src/core/main/initResponseFromAddress/makeVoterInfoQuery');

jest.mock('../../../src/core/main/initResponseFromAddress/makeVoterInfoQuery');

describe('initResponseFromAddress', () => {
  beforeAll(() => {
    makeVoterInfoQuery.mockReturnValue(resolve({}));
  });

  it('returns a Fluture compatible future', () => {
    expect(isFuture(initResponseFromAddress('123 Sesame Street'))).toBeTruthy();
  });

  it('resolves with a response object', done => {
    expect.assertions(1);
    const onFailure = jest.fn(() => { done(); });
    const onSuccess = res => {
      expect(res).toMatchObject({
        headers: expect.any(Object),
        statusCode: expect.any(Number),
        body: expect.any(Object)
      });
      done();
    };

    initResponseFromAddress('123 Sesame Street').pipe(fork(onFailure)(onSuccess));
  });
});