'use strict';

const makeMatchParser = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/getVoteSmartCandidateID/makeMatchParser');
const singleCandidate = require('../../mockData/singleCandidate');
const multipleCandidates = require('../../mockData/multipleCandidates');
const duplicateCandidates = require('../../mockData/duplicateCandidates');
const similarCandidates = require('../../mockData/similarCandidates');

describe('makeMatchParser', () => {
  it('returns a function', () => {
    expect(typeof makeMatchParser({ name: { first: 'John' } } ) === 'function').toBeTruthy();
  });

  it('returns a function that returns the candidate object if a single match is found', () => {
    expect(makeMatchParser({
      name: {
        first: 'Donald'
      }
    })(singleCandidate)).toMatchObject(singleCandidate.data.candidateList.candidate);
  });

  it('returns a function that correctly parses and returns the correct candidate given multiple matches', () => {
    expect(makeMatchParser({
      name: {
        first: 'Amanda'
      }
    })(multipleCandidates)).toMatchObject(multipleCandidates.data.candidateList.candidate[0]);
  });

  it('returns a function that correctly parses and returns the correct candidate given duplicate correct matches', () => {
    expect(makeMatchParser({
      name: {
        first: 'John'
      }
    })(duplicateCandidates)).toMatchObject(duplicateCandidates.data.candidateList.candidate[0]);
  });

  it('returns a function that returns an empty object if it is unable to parse a match from many similar candidates', () => {
    expect(makeMatchParser({
      name: {
        first: 'John'
      }
    })(similarCandidates)).toEqual({});
  });

  it('returns a function that returns an empty object if no matches are found', () => {
    expect(makeMatchParser({
      name: {
        first: 'John'
      }
    })({
      status: 200,
      data: { "error": { "errorMessage": "No candidates found matching this criteria." } }
    })).toEqual({});
  });

  it('returns a function that returns an empty object if the API request fails', () => {
    expect(makeMatchParser({
      name: {
        first: 'John'
      }
    })({
      status: 400
    })).toEqual({});
  });
});