'use strict';

const mapSymbolToVote = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/addCandidateVotesAndContribs/getCandidateVotes/parseCandidateVotes/mapSymbolToVote');

describe('mapSymbolToVote', () => {
  it('returns the correct vote for each VoteSmart symbol', () => {
    expect(mapSymbolToVote('-')).toBe('Abstained');
    expect(mapSymbolToVote('C')).toBe('Co-sponsor');
    expect(mapSymbolToVote('N')).toBe('No');
    expect(mapSymbolToVote('P')).toBe('Signed');
    expect(mapSymbolToVote('S')).toBe('Sponsor');
    expect(mapSymbolToVote('V')).toBe('Vetoed');
    expect(mapSymbolToVote('Y')).toBe('Yes');
  });

  it('returns the input as a string if given an invalid input', () => {
    expect(mapSymbolToVote('G')).toBe('G');
    expect(mapSymbolToVote(400)).toBe('400');
  });
});