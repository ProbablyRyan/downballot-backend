'use strict';

const { isFuture, fork } = require('fluture');
const getCandidateVotes = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/addCandidateVotesAndContribs/getCandidateVotes');
const mainGetter = require('../../../src/core/utils/asyncIO/mainGetter');
const mockResponse = require('../../mockData/candidateVotes.json');

jest.mock('../../../src/core/utils/asyncIO/mainGetter');

describe('getCandidateVotes', () => {
  beforeEach(() => {
    mainGetter.mockReset();
  });

  it('returns a Fluture compatible future', () => {
    expect(isFuture(getCandidateVotes({}))).toBeTruthy();
  });

  it('resolves with a parsed votes object given a valid candidate', done => {
    expect.assertions(2);
    mainGetter.mockResolvedValue(mockResponse);
    const onFailure = jest.fn(() => { done(); });
    const onSuccess = res => {
      expect(onFailure).not.toBeCalled();
      expect(res).toMatchObject({
        votes: {
          list: expect.any(Array)
        }
      });
      done();
    };

    getCandidateVotes({ voteSmartID: '12345' }).pipe(fork(onFailure)(onSuccess));
  });

  it('resolves with an empty object if the API request returns no votes', done => {
    expect.assertions(2);
    mainGetter.mockResolvedValue({ status: 200, data: { error: { errorMessage: 'Error' }}});
    const onFailure = jest.fn(() => { done(); });
    const onSuccess = res => {
      expect(onFailure).not.toBeCalled();
      expect(res).toEqual({});
      done();
    };

    getCandidateVotes({ voteSmartID: '12345' }).pipe(fork(onFailure)(onSuccess));
  });

  it('resolves with an empty object if the API request fails', done => {
    expect.assertions(2);
    mainGetter.mockResolvedValue({ status: 400 });
    const onFailure = jest.fn(() => { done(); });
    const onSuccess = res => {
      expect(onFailure).not.toBeCalled();
      expect(res).toEqual({});
      done();
    };

    getCandidateVotes({ voteSmartID: '12345' }).pipe(fork(onFailure)(onSuccess));
  });
});