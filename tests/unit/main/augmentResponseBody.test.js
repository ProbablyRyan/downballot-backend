'use strict';

const { isFuture, resolve, fork } = require('fluture');
const augmentResponseBody = require('../../../src/core/main/augmentResponseBody');
const retrieveContest = require('../../../src/core/main/augmentResponseBody/retrieveContest');
const Response = require('../../../src/core/utils/Response');

jest.mock('../../../src/core/main/augmentResponseBody/retrieveContest');

describe('augmentResponseBody', () => {
  beforeEach(() => {
    retrieveContest.mockReset();
  });

  it('returns a Fluture compatible future', () => {
    retrieveContest.mockReturnValue(resolve());
    expect(isFuture(augmentResponseBody(Response({
      statusCode: 200,
      body: {
        contests: {
          augmented: [0]
        }
      }
    })))).toBeTruthy();
  });

  it('resolves with a response object with body.contests.augmented set to the value returned from "retrieveContest"', done => {
    retrieveContest.mockReturnValue(resolve('Success'));
    expect.assertions(4);
    
    const someArray = ['Failure'];
    const onFailure = jest.fn(() => { done(); });
    const onSuccess = res => {
      expect(onFailure).not.toBeCalled();
      expect(res.body.contests.augmented[0]).toEqual('Success');
      expect(res.body.contests.augmented.length).toBe(1);
      expect(res.body.contests.augmented).not.toBe(someArray);
      done();
    };

    augmentResponseBody(Response({
      statusCode: 200,
      body: {
        contests: {
          augmented: someArray
        }
      }
    })).pipe(fork(onFailure)(onSuccess));
  });

  it('resolves with the input response if the response body does not contain augmented contests', done => {
    retrieveContest.mockReturnValue(resolve());
    expect.assertions(1);
    const input = Response({ statusCode: 404 });

    const onFailure = jest.fn(() => { done(); });
    const onSuccess = res => {
      expect(res).toBe(input);
      done();
    };

    augmentResponseBody(input).pipe(fork(onFailure)(onSuccess));
  });
});