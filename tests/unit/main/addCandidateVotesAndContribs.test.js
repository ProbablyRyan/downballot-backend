'use strict';

const { isFuture, resolve, fork } = require('fluture');
const addCandidateVotesAndContribs = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/addCandidateVotesAndContribs');
const getCandidateVotes = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/addCandidateVotesAndContribs/getCandidateVotes');
const scrapeCandidateContribs = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/addCandidateVotesAndContribs/scrapeCandidateContribs');

jest.mock('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/addCandidateVotesAndContribs/getCandidateVotes');
jest.mock('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/addCandidateVotesAndContribs/scrapeCandidateContribs');

const mockCandidate = {
  voteSmartID: '12345',
  name: {
    first: 'John',
    last: 'Doe'
  }
};

describe('addCandidateVotesAndContribs', () => {
  beforeEach(() => {
    getCandidateVotes.mockReset();
    scrapeCandidateContribs.mockReset();
  });

  it('returns a Fluture compatible future', () => {
    getCandidateVotes.mockReturnValue(resolve({}));
    scrapeCandidateContribs.mockReturnValue(resolve({}));
    expect(isFuture(addCandidateVotesAndContribs(mockCandidate))).toBeTruthy();
  });

  it('resolves with a new candidate object with the voteSmartID prop removed, and votes and contribs lists and sources set', done => {
    expect.assertions(3);
    getCandidateVotes.mockReturnValue(resolve({
      votes: {
        list: []
      }
    }));
    scrapeCandidateContribs.mockReturnValue(resolve({
      contributors: {
        list: []
      }
    }));
    const onFailure = jest.fn(() => { done(); });
    const onSuccess = res => {
      expect(onFailure).not.toBeCalled();
      expect(res).not.toBe(mockCandidate);
      expect(res).toMatchObject({
        name: {
          first: 'John',
          last: 'Doe'
        },
        votes: {
          list: expect.any(Array),
          source: expect.any(String)
        },
        contributors: {
          list: expect.any(Array),
          source: expect.any(String)
        }
      });

      done();
    };

    addCandidateVotesAndContribs(mockCandidate).pipe(fork(onFailure)(onSuccess));
  });

  it('resolves with a new candidate object with the voteSmartID prop removed if the votes and contribs are not found', done => {
    expect.assertions(3);
    getCandidateVotes.mockReturnValue(resolve({}));
    scrapeCandidateContribs.mockReturnValue(resolve({}));
    const onFailure = jest.fn(() => { done(); });
    const onSuccess = res => {
      expect(onFailure).not.toBeCalled();
      expect(res).not.toBe(mockCandidate);
      expect(res).toMatchObject({
        name: {
          first: 'John',
          last: 'Doe'
        }
      });

      done();
    };

    addCandidateVotesAndContribs(mockCandidate).pipe(fork(onFailure)(onSuccess));
  });
});