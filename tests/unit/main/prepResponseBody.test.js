'use strict';

const prepResponseBody = require('../../../src/core/main/initResponseFromAddress/prepResponseBody');
const Response = require('../../../src/core/utils/Response');
const mockBallot = require('../../mockData/mockBallot.json');

describe('prepResponseBody', () => {
  it('returns a response object with body prop set to a filtered copy of the input response body', () => {
    const mockResponse = Response({ statusCode: 200, body: mockBallot });
    const res = prepResponseBody(mockResponse);

    expect(res).toEqual({
      headers: {
        "Access-Control-Allow-Origin" : process.env.CORS_ORIGIN
      },
      statusCode: 200,
      body: expect.any(Object)
    });

    expect(res.body.contests.augmented[0]).toEqual({
      candidates: expect.any(Array),
      district: expect.any(Object),
      office: expect.any(String)
    });

    expect(res.body.contests.augmented[0].candidates[0]).toEqual({
      candidateUrl: expect.any(String),
      channels: expect.any(Array),
      name: expect.any(String),
      party: expect.any(String)
    });

    expect(res.body.contests.basic[0]).toEqual({
      district: expect.any(Object),
      office: expect.any(String)
    });

    expect(res.body.contests.referenda[0]).toEqual({
      referendumSubtitle: expect.any(String),
      referendumTitle: expect.any(String),
      referendumUrl: expect.any(String)
    });

    expect(res.body).not.toBe(mockResponse);
  });
});