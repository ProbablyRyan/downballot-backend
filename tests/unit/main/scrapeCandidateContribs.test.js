'use strict';

const { isFuture, fork } = require('fluture');
const fs = require('fs');
const path = require('path');
const scrapeCandidateContribs = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/addCandidateVotesAndContribs/scrapeCandidateContribs');
const mainGetter = require('../../../src/core/utils/asyncIO/mainGetter');

jest.mock('../../../src/core/utils/asyncIO/mainGetter');

describe('scrapeCandidateContribs', () => {
  beforeEach(() => {
    mainGetter.mockReset();
  });

  it('returns a Fluture compatible future', () => {
    expect(isFuture(scrapeCandidateContribs({
      voteSmartID: '12345',
      name: {
        first: 'John',
        last: 'Doe'
      }
    }))).toBeTruthy();
  });

  it('resolves with a parsed contributors object if a Top Contributors tables exists', done => {
    expect.assertions(2);
    fs.readFile(
      path.resolve(__dirname, '../../mockData/aoc.html'),
      'utf8',
      (err, financePage) => {
        mainGetter.mockResolvedValue({ status: 200, data: financePage });
        const onFailure = jest.fn(() => { done(); });
        const onSuccess = res => {
          expect(onFailure).not.toBeCalled();
          expect(res).toMatchObject({
            contributors: {
              list: expect.any(Array)
            }
          });
          done();
        };

        scrapeCandidateContribs({
          voteSmartID: '12345',
          name: {
            first: 'John',
            last: 'Doe'
          }
        }).pipe(fork(onFailure)(onSuccess));
      }
    );
  });

  it('resolves with an empty object if a Top Contributors table does not exist', done => {
    expect.assertions(2);
    fs.readFile(
      path.resolve(__dirname, '../../mockData/aoc_old.html'),
      'utf8',
      (err, financePage) => {
        mainGetter.mockResolvedValue({ status: 200, data: financePage });
        const onFailure = jest.fn(() => { done(); });
        const onSuccess = res => {
          expect(onFailure).not.toBeCalled();
          expect(res).toEqual({});
          done();
        };

        scrapeCandidateContribs({
          voteSmartID: '12345',
          name: {
            first: 'John',
            last: 'Doe'
          }
        }).pipe(fork(onFailure)(onSuccess));
      }
    );
  });

  it('resolves with an empty object if the http request fails', done => {
    expect.assertions(2);
    mainGetter.mockResolvedValue({ status: 400 });
    const onFailure = jest.fn(() => { done(); });
    const onSuccess = res => {
      expect(onFailure).not.toBeCalled();
      expect(res).toEqual({});
      done();
    };

    scrapeCandidateContribs({
      voteSmartID: '12345',
      name: {
        first: 'John',
        last: 'Doe'
      }
    }).pipe(fork(onFailure)(onSuccess));
  });
});