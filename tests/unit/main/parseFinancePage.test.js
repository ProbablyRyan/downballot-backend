'use strict';

const fs = require('fs');
const path = require('path');
const parseFinancePage = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/addCandidateVotesAndContribs/scrapeCandidateContribs/parseFinancePage');

describe('parseFinancePage', () => {
  it('returns the top five contributors and amounts if the Top Contributors tables exists', done => {
    expect.assertions(2);

    fs.readFile(
      path.resolve(__dirname, '../../mockData/aoc.html'),
      'utf8',
      (err, financePage) => {
        if (err) {
          throw err;
        }

        const parsedContribs = parseFinancePage({ status: 200, data: financePage });

        expect(parsedContribs).toMatchObject({
          contributors: {
            list: expect.any(Array)
          }
        });
        expect(parsedContribs.contributors.list).toHaveLength(5);

        done();
      }
    );
  });

  it('returns an empty object if a Top Contributors table is not present', done => {
    expect.assertions(1);

    fs.readFile(
      path.resolve(__dirname, '../../mockData/aoc_old.html'),
      'utf8',
      (err, financePage) => {
        if (err) {
          throw err;
        }

        const parsedPage = parseFinancePage({ status: 200, data: financePage });

        expect(parsedPage).toEqual({});

        done();
      }
    );
  });
});