'use strict';

const { isFuture } = require('fluture');
const queryCachedContest = require('../../../src/core/main/augmentResponseBody/retrieveContest/queryCachedContest');
const mockContest = require('../../mockData/testContest');

describe('queryCahcedContest', () => {
  it('returns a Fluture compatible future', () => {
    expect(isFuture(queryCachedContest(mockContest))).toBeTruthy();
  });
});