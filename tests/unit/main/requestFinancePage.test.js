'use strict';

const { isFuture } = require('fluture');
const requestFinancePage = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/addCandidateVotesAndContribs/scrapeCandidateContribs/requestFinancePage');

describe('requestFinancePage', () => {
  it('returns a Fluture compatible future', () => {
    expect(isFuture(requestFinancePage({
      voteSmartID: '12345',
      name: {
        first: 'John',
        last: 'Doe'
      }
    }))).toBeTruthy();
  });
});