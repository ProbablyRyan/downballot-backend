'use strict';

const { fork, isFuture, resolve, reject } = require('fluture');
const retrieveContest = require('../../../src/core/main/augmentResponseBody/retrieveContest');
const augmentNewContest = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest');
const queryCachedContest = require('../../../src/core/main/augmentResponseBody/retrieveContest/queryCachedContest');
const mockContest = require('../../mockData/testContest');

jest.mock('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest');
jest.mock('../../../src/core/main/augmentResponseBody/retrieveContest/queryCachedContest');

describe('retrieveContest', () => {
  beforeEach(() => {
    augmentNewContest.mockReset();
    queryCachedContest.mockReset();
  });
  
  it('returns a Fluture compatible future', () => {
    augmentNewContest.mockReturnValue(resolve());
    queryCachedContest.mockReturnValue(resolve());
    expect(isFuture(retrieveContest(mockContest))).toBeTruthy();
  });

  it('resolves with the value returned from "queryCachedContest" if given a previously augmented contest', done => {
    expect.assertions(1);
    augmentNewContest.mockReturnValue(resolve());
    queryCachedContest.mockReturnValue(resolve('Cached Contest'));
    
    const onFailure = jest.fn(() => { done(); });
    const onSuccess = res => {
      expect(res).toEqual('Cached Contest');
      done();
    };

    retrieveContest(mockContest).pipe(fork(onFailure)(onSuccess));
  });

  it('resolves with the value returned from "augmentNewContest" if "queryCachedContest" fails', done => {
    expect.assertions(1);
    augmentNewContest.mockReturnValue(resolve('New Contest'));
    queryCachedContest.mockReturnValue(reject());
    
    const onFailure = jest.fn(() => { done(); });
    const onSuccess = res => {
      expect(res).toEqual('New Contest');
      done();
    };

    retrieveContest(mockContest).pipe(fork(onFailure)(onSuccess));
  });
});