'use strict';

const initResponseFromQuery = require('../../../src/core/main/initResponseFromAddress/initResponseFromQuery');

describe('initResponseFromBody', () => {
  it('returns a 200 response object with body if the query was successful', () => {
    const mockSuccess = {
      status: 200,
      data: {
        contests: []
      }
    };

    expect(initResponseFromQuery(mockSuccess)).toEqual({
      headers: expect.any(Object),
      statusCode: 200,
      body: {
        ...mockSuccess.data
      }
    });
  });

  it('reutrns a 404 response object if the query returns no candidates', () => {
    const mockFailure = {
      status: 200,
      data: {}
    };

    expect(initResponseFromQuery(mockFailure)).toEqual({
      headers: expect.any(Object),
      statusCode: 404,
      body: {}
    });
  });

  it('returns a 404 response object if the query fails', () => {
    const mockError = {
      status: 400
    };

    expect(initResponseFromQuery(mockError)).toEqual({
      headers: expect.any(Object),
      statusCode: 404,
      body: {}
    });
  });
});