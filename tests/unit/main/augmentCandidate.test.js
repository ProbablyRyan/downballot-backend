'use strict';

const { isFuture, resolve, fork, reject } = require('fluture');
const augmentCandidate = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate');
const getVoteSmartCandidateID = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/getVoteSmartCandidateID');
const getCandidateVotes = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/addCandidateVotesAndContribs/getCandidateVotes');
const scrapeCandidateContribs = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/addCandidateVotesAndContribs/scrapeCandidateContribs');

jest.mock('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/getVoteSmartCandidateID');
jest.mock('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/addCandidateVotesAndContribs/getCandidateVotes');
jest.mock('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/addCandidateVotesAndContribs/scrapeCandidateContribs');

describe('augmentCandidate', () => {
  beforeEach(() => {
    getVoteSmartCandidateID.mockReset();
    getCandidateVotes.mockReset();
    scrapeCandidateContribs.mockReset();
  });

  it('returns a Fluture compatible future', () => {
    getVoteSmartCandidateID.mockReturnValue(resolve({}));
    getCandidateVotes.mockReturnValue(resolve({}));
    scrapeCandidateContribs.mockReturnValue(resolve({}));
    expect(isFuture(augmentCandidate({ name: { last: 'Doe' } }))).toBeTruthy();
  });

  it('resolves with an augmented candidate object if a VoteSmart ID is found', done => {
    expect.assertions(3);
    getVoteSmartCandidateID.mockImplementation(candidate => resolve({
      ...candidate,
      voteSmartID: '12345'
    }));
    getCandidateVotes.mockReturnValue(resolve({
      votes: {
        list: []
      }
    }));
    scrapeCandidateContribs.mockReturnValue(resolve({
      contributors: {
        list: []
      }
    }));
    
    const mockCandidate = { name: { first: 'John', last: 'Doe' } };

    const onFailure = jest.fn(() => { done(); });
    const onSuccess = res => {
      expect(onFailure).not.toBeCalled();
      expect(res).not.toBe(mockCandidate);
      expect(res).toMatchObject({
        name: {
          first: expect.any(String),
          last: expect.any(String)
        },
        votes: {
          list: expect.any(Array),
          source: expect.any(String)
        },
        contributors: {
          list: expect.any(Array),
          source: expect.any(String)
        }
      });

      done();
    };

    augmentCandidate(mockCandidate).pipe(fork(onFailure)(onSuccess));
  });

  it('resolves with the input candidate object if no VoteSmart ID is found', done => {
    expect.assertions(2);
    getVoteSmartCandidateID.mockImplementation(candidate => reject());
    getCandidateVotes.mockReturnValue(resolve({
      votes: {
        list: []
      }
    }));
    scrapeCandidateContribs.mockReturnValue(resolve({
      contributors: {
        list: []
      }
    }));

    const mockCandidate = { name: { first: 'John', last: 'Doe' } };
    const onFailure = jest.fn(() => { done(); });
    const onSuccess = res => {
      expect(onFailure).not.toBeCalled();
      expect(res).toBe(mockCandidate);
      done();
    };

    augmentCandidate(mockCandidate).pipe(fork(onFailure)(onSuccess));
  });
});
