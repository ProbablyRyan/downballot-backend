'use strict';

const parseCandidateVotes = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/addCandidateVotesAndContribs/getCandidateVotes/parseCandidateVotes');
const mockResponse = require('../../mockData/candidateVotes.json');

describe('parseCandidateVotes', () => {
  it('returns the title and vote of the top 5 bills given a valid input', () => {
    const parsedVotes = parseCandidateVotes(mockResponse);

    expect(parsedVotes).toMatchObject({
      votes: {
        list: expect.any(Array)
      }
    });
    expect(parsedVotes.votes.list).toHaveLength(5);
  });
});