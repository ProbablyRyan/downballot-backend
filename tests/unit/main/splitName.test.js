'use strict';

const splitName = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/splitName');

describe('splitName', () => {
    it('returns a new object', () => {
      const candidate = {
        name: 'Van Taylor'
      };
  
      const splitCandidate = splitName(candidate);
  
      expect(splitCandidate !== candidate).toBeTruthy();
      expect(splitCandidate).toMatchObject({
        name: {
          full: 'Van Taylor',
          first: 'Van',
          last: 'Taylor'
        }
      });
    });
  
    it('correctly parses last names from names with generational suffixes', () => {
      const candidate1 = {
        name: 'John Doe Sr.'
      };
  
      const candidate2 = {
        name: 'John Doe Jr.'
      };
  
      const candidate3 = {
        name: 'John Doe IV'
      };
  
      const split1 = splitName(candidate1);
      const split2 = splitName(candidate2);
      const split3 = splitName(candidate3);
  
      expect(split1.name.last).toBe('Doe');
      expect(split2.name.last).toBe('Doe');
      expect(split3.name.last).toBe('Doe');
    });
  
    it('normalizes diacritics', () => {
      const candidate = {
        name: 'Chrysta Castañeda'
      };
      const splitCandidate = splitName(candidate);
  
      expect(splitCandidate.name.full).toBe('Chrysta Castaneda');
      expect(splitCandidate.name.last).toBe('Castaneda');
    });
  
    it('parses the primary candidate name from split tickets', () => {
      const candidate = {
        name: 'Joe Biden/Kamala Harris'
      };
      const splitCandidate = splitName(candidate);
  
      expect(splitCandidate).toMatchObject({
        name: {
          full: 'Joe Biden',
          first: 'Joe',
          last: 'Biden'
        }
      });
    });
  });