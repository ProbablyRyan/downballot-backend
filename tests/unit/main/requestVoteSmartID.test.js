'use strict';

const { isFuture } = require('fluture');
const requestVoteSmartID = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/getVoteSmartCandidateID/requestVoteSmartID');

describe('requestVoteSmartID', () => {
  it('returns a Fluture compatible future', () => {
    expect(isFuture(requestVoteSmartID({ name: { last: 'Doe' } }))).toBeTruthy();
  });
});