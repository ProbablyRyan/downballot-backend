'use strict';

const { isFuture, resolve, fork } = require('fluture');
const getVoteSmartCandidateID = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/getVoteSmartCandidateID');
const requestVoteSmartID = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/getVoteSmartCandidateID/requestVoteSmartID');

const mockCandidate = { name: { first: 'Amanda', last: 'Jennings' } };
const mockResponse = require('../../mockData/multipleCandidates');
jest.mock('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/getVoteSmartCandidateID/requestVoteSmartID');

describe('getVoteSmartCandidateID', () => {
  beforeEach(() => {
    requestVoteSmartID.mockReset();
  });

  it('returns a Fluture compatible future', () => {
    requestVoteSmartID.mockReturnValue(resolve(mockResponse));
    expect(isFuture(getVoteSmartCandidateID(mockCandidate))).toBeTruthy();
  });

  it('resolves with a new candidate object with an added voteSmartID prop if a match is found', done => {
    expect.assertions(3);
    requestVoteSmartID.mockReturnValue(resolve(mockResponse));
    const onFailure = jest.fn(() => { done(); });
    const onSuccess = res => {
      expect(onFailure).not.toBeCalled();
      expect(res).not.toBe(mockCandidate);
      expect(res.hasOwnProperty('voteSmartID')).toBeTruthy();
      done();
    };

    getVoteSmartCandidateID(mockCandidate).pipe(fork(onFailure)(onSuccess));
  });

  it('rejects if no match is found', done => {
    expect.assertions(1);
    requestVoteSmartID.mockReturnValue(resolve({}));
    const onFailure = () => {
      expect(onSuccess).not.toBeCalled();
      done();
    };
    const onSuccess = jest.fn(() => { done(); });

    getVoteSmartCandidateID(mockCandidate).pipe(fork(onFailure)(onSuccess));
  });
});