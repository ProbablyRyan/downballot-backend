'use strict';

const { isFuture, resolve, fork } = require('fluture');
const augmentNewContest = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest');
const augmentCandidate = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate');
const mockContest = require('../../mockData/testContest');

jest.mock('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate');

describe('augmentNewContest', () => {
  beforeEach(() => {
    augmentCandidate.mockReset();
  });

  it('returns a Fluture compatible future', () => {
    augmentCandidate.mockReturnValue(resolve({}));
    expect(isFuture(augmentNewContest(mockContest))).toBeTruthy();
  });

  it('resolves to a new contest object with augmented candidates', done => {
    expect.assertions(3);
    augmentCandidate.mockImplementation(candidate => resolve({
      ...candidate,
      contributors: {
        list: [],
        source: ''
      },
      votes: {
        list: [],
        source: ''
      }
    }));

    const onFailure = jest.fn(() => { done(); });
    const onSuccess = res => {
      expect(onFailure).not.toBeCalled();
      expect(res).not.toBe(mockContest);
      expect(res).toMatchObject({
        ...mockContest,
        candidates: expect.any(Array)
      });
      done();
    };

    augmentNewContest(mockContest).pipe(fork(onFailure)(onSuccess));
  });
});
