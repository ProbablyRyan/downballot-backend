'use strict';

const { isFuture } = require('fluture');
const queryCandidateVotes = require('../../../src/core/main/augmentResponseBody/retrieveContest/augmentNewContest/augmentCandidate/addCandidateVotesAndContribs/getCandidateVotes/queryCandidateVotes');

describe('queryCandidateVotes', () => {
  it('returns a Fluture compatible future', () => {
    expect(isFuture(queryCandidateVotes({ voteSmartID: '12345 '}))).toBeTruthy();
  });
});