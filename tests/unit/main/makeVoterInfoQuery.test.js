'use strict';

const { isFuture } = require('fluture');
const makeVoterInfoQuery = require('../../../src/core/main/initResponseFromAddress/makeVoterInfoQuery');

describe('makeVoterInfoQuery', () => {
  it('returns a Fluture compatible future', () => {
    expect(isFuture(makeVoterInfoQuery('123 Sesame Street'))).toBeTruthy();
  });
});