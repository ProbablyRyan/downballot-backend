# Downballot Back End

Back end services for the [Downballot web app](https://gitlab.com/ProbablyRyan/downballot-spa). Combines data from the [Google Civic Information Api](https://developers.google.com/civic-information/) and [VoteSmart.org](https://justfacts.votesmart.org/) to provide users with campaign funding information and the voting histories of candidates on their local ballots.

## Built With

* [Fluture](https://github.com/fluture-js/Fluture#readme)
* [Ramda](https://ramdajs.com/docs/)
* [lodash](https://lodash.com/docs/4.17.15)
* [cheerio](https://cheerio.js.org/)
* [mongoose](https://mongoosejs.com/docs/guide.html)
* [Serverless Framework](https://www.serverless.com/framework/docs/)

## Quick Start

### Initial Setup

1. Clone the repo

```sh
git clone git@gitlab.com:ProbablyRyan/downballot-backend.git
```

2. Install Serverless and Babel

```sh
npm install -g serverless @babel/core @babel/cli
```

3. Install dependencies

```sh
npm install
```

### Scripts

```build```

Transpiles code in ```src/``` using Babel and pipes the output to ```dist/```. Much of the project makes use of JavaScript's [optional chaining operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Optional_chaining), so this transpile step is necessary until AWS supports Node 14+.

```test```

Runs the test suite. Tests are written with [Jest](https://jestjs.io/docs/en/getting-started.html).

```watch```

Runs Jest with the ```--watch``` flag.

```coverage```

Runs Jest with the ```--coverage``` flag.

```lint```

Runs ESLint on the ```config/``` and ```src/``` folders.

```serve```

Serves the contents of ```dist/``` locally using [serverless-offline](https://www.serverless.com/plugins/serverless-offline).
