'use strict';

const Response = require('../core/utils/Response');
const demoData = require('../demo/demoData');

exports.handler = function(event, context, callback) {
  context.callbackWaitsForEmptyEventLoop = false;
  setTimeout(() => {
    callback(
      null,
      Response({
        statusCode: 200,
        body: demoData
      }).bodyAsJSON()
    );
  }, 1500);
};