'use strict';

const { fork, chain } = require('fluture');
const mongoose = require('mongoose');
const Response = require('../core/utils/Response');
const initResponseFromAddress = require('../core/main/initResponseFromAddress');
const augmentResponseBody = require('../core/main/augmentResponseBody');
const upsertContests = require('../core/main/upsertContests');
const db = process.env.MONGO_URI;

mongoose.connect(db, {
  bufferMaxEntries: 0,
  serverSelectionTimeoutMS: 5000,
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true
}).catch(err => {
  console.log(err);
});

exports.handler = function(event, context, callback) {
  context.callbackWaitsForEmptyEventLoop = false;
  if (event?.queryStringParameters?.address) {
    initResponseFromAddress(event.queryStringParameters.address)
      .pipe(chain(augmentResponseBody))
      .pipe(fork(() => {
        callback(null, Response({ statusCode: 500 }).bodyAsJSON());
      })(res => {
        upsertContests(res.getBody());
        callback(null, res.bodyAsJSON());
      }));
  } else {
    callback(null, Response({ statusCode: 400 }).bodyAsJSON());
  }
};