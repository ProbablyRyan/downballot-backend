module.exports = {
  "election": {
    "name": "Downballot Demo Election",
    "electionDay": "1970-01-01"
  },
  "contests": {
    "augmented": [
      {
        "office": "New Mexico Supreme Court Judge",
        "district": {
          "name": "New Mexico"
        },
        "candidates": [
          {
            "name": {
              "first": "Saul",
              "last": "Goodman",
              "full": "Saul Goodman"
            },
            "candidateUrl": "https://www.imdb.com/title/tt0903747/",
            "channels": [
              {
                "type": "YouTube",
                "id": "https://www.youtube.com/watch?v=pPd67CEL54E"
              }
            ],
            "votes": {},
            "contributors": {
              "list": [
                {
                  "name": "Vamonos Pest",
                  "amount": "$979,056.00"
                },
                {
                  "name": "Los Pollos Hermanos",
                  "amount": "$918,016.00"
                },
                {
                  "name": "Madrigal Electromotive GmbH",
                  "amount": "$788,508.00"
                },
                {
                  "name": "Hamlin, Hamlin & McGill",
                  "amount": "$442,786.00"
                },
                {
                  "name": "University of American Samoa",
                  "amount": "$430,058.00"
                }
              ],
              "source": "https://justfacts.votesmart.org/"
            }
          },
          {
            "name": {
              "first": "Bob",
              "last": "Loblaw",
              "full": "Bob Loblaw"
            },
            "candidateUrl": "https://www.imdb.com/title/tt0367279/",
            "channels": [
              {
                "type": "YouTube",
                "id": "https://www.youtube.com/watch?v=YfRKv3JDZO4"
              }
            ],
            "votes": {},
            "contributors": {
              "list": [
                {
                  "name": "Bluth's Original Frozen Banana Stand",
                  "amount": "$979,056.00"
                },
                {
                  "name": "Austero Bluth Company",
                  "amount": "$918,016.00"
                },
                {
                  "name": "Sitwell Enterprises",
                  "amount": "$788,508.00"
                },
                {
                  "name": "Michael B. Company",
                  "amount": "$442,786.00"
                },
                {
                  "name": "Gobias Industries",
                  "amount": "$430,058.00"
                }
              ],
              "source": "https://justfacts.votesmart.org/"
            }
          },
          {
            "name": {
              "first": "Jackie",
              "last": "Chiles",
              "full": "Jackie Chiles"
            },
            "votes": {},
            "contributors": {}
          }
        ]
      },
      {
        "office": "President of the Agricultural Hall",
        "district": {
          "name": "Letterkenny, ON"
        },
        "candidates": [
          {
            "name": {
              "first": "",
              "last": "McMurray",
              "full": "McMurray"
            },
            "votes": {},
            "contributors": {}
          },
          {
            "name": {
              "first": "Wayne",
              "last": "",
              "full": "Wayne"
            },
            "votes": {},
            "contributors": {}
          }
        ]
      }
    ],
    "basic": [
      {
        "office": "School Mascot",
        "district": {
          "name": "South Park Elementary"
        }
      }
    ],
    "referenda": [
      {
        "referendumUrl": "https://howjavascriptworks.com/",
        "referendumTitle": "CHANGE THE SPELLING FOR '1' FROM 'ONE' TO 'WUN' - VOTE YES OR NO",
        "referendumSubtitle": "The word for 1 is misspelled. The pronunciation of one does not conform to any of the standard or special rules of English pronunciation. And having the word for 1 start with the letter that looks like 0 is a bug."
      }
    ]
  }
}