'use strict';

const { map, parallel, resolve } = require('fluture');
const { set } = require('ramda');
const retrieveContest = require('./augmentResponseBody/retrieveContest');
const { augmentedContests } = require('../helpers/lenses');

module.exports = function augmentResponseBody(response) {
  return response?.body?.contests?.augmented?.length > 0
    ? parallel
      (response.body.contests.augmented.length)
      (response.body.contests.augmented.map(retrieveContest))
      .pipe(map(res =>
        response.setBody(set(augmentedContests, res, response.body))
      ))
    : resolve(response);
};