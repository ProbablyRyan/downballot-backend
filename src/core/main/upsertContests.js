'use strict';

const { noop } = require('lodash');
const mongoose = require('mongoose');
const Contest = require('../db/models/Contest');
const contestQueryFilter = require('../db/queryFilters/contestQueryFilter');

module.exports = function upsertContests(ballot) {
  try {
    if (mongoose?.connection?.readyState === 1) {
      const expiry = new Date(ballot.election.electionDay + 'Z').valueOf() + 86400000;

      return ballot.contests.augmented.map(contest =>
        Contest.updateOne(
          contestQueryFilter(contest),
          {
            ...contest,
            expireAt: expiry
          },
          {
            upsert: true
          },
          noop
        )
      );
    } else {
      return [];
    }
  } catch (e) {
    return [];
  }
};