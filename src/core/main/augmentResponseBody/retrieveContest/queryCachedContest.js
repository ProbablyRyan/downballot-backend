'use strict';

const { encaseP, map } = require('fluture');
const Contest = require('../../../db/models/Contest');
const contestQueryFilter = require('../../../db/queryFilters/contestQueryFilter');

module.exports = function queryCachedContest(contest) {
  return encaseP(queryParams => Contest
    .findOne(queryParams)
    .select('-_id -__v -expireAt')
    .orFail()
    .exec()
  )(contestQueryFilter(contest))
  .pipe(map(res => res.toObject()));
};