'use strict';

const { bichain, resolve } = require('fluture');
const getVoteSmartCandidateID = require('./augmentCandidate/getVoteSmartCandidateID');
const addCandidateVotesAndContribs = require('./augmentCandidate/addCandidateVotesAndContribs');

module.exports = function augmentCandidate(candidate) {
  return getVoteSmartCandidateID(candidate)
    .pipe(bichain(() => resolve(candidate))(addCandidateVotesAndContribs));
};