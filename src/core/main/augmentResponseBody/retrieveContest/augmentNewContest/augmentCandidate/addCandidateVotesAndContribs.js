'use strict';

const { both, map } = require('fluture');
const { dissoc, pipe } = require('ramda');
const getCandidateVotes = require('./addCandidateVotesAndContribs/getCandidateVotes');
const scrapeCandidateContribs = require('./addCandidateVotesAndContribs/scrapeCandidateContribs');
const condSet = require('../../../../../utils/condSet');
const urlMaker = require('../../../../../utils/urlMaker');
const { contribsSource, votesSource } = require('../../../../../helpers/lenses');

const makeContribsUrl = urlMaker('https://justfacts.votesmart.org/candidate/campaign-finance');
const makeVotesSourceUrl = urlMaker('https://justfacts.votesmart.org/candidate/key-votes');

const hasContribsProp = obj => obj.hasOwnProperty('contributors');
const hasVotesProp = obj => obj.hasOwnProperty('votes');

const condSetContribsSource = condSet(hasContribsProp)(contribsSource);
const condSetVotesSource = condSet(hasVotesProp)(votesSource);

const condSetContribsUrl = pipe(makeContribsUrl, condSetContribsSource);
const condSetVotesUrl = pipe(makeVotesSourceUrl, condSetVotesSource);

module.exports = function addCandidateVotesAndContribs(candidate) {
  const urlParams = [
    `${candidate.voteSmartID}`,
    `${candidate.name.first}-${candidate.name.last}`
  ];

  return both(getCandidateVotes(candidate))(scrapeCandidateContribs(candidate))
    .pipe(map(([votes, contribs]) => Object.assign(
      {},
      dissoc('voteSmartID', candidate),
      condSetContribsUrl(urlParams)(contribs),
      condSetVotesUrl(urlParams)(votes)
    )));
};