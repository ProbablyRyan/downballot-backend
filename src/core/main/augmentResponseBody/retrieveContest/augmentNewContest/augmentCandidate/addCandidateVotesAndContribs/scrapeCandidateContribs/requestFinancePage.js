'use strict';

const { encaseP } = require('fluture');
const { pipe } = require('ramda');
const mainGetter = require('../../../../../../../utils/asyncIO/mainGetter');
const urlMaker = require('../../../../../../../utils/urlMaker');

const makeContribsUrl = candidate => urlMaker('https://justfacts.votesmart.org/candidate/campaign-finance')([
  `${candidate.voteSmartID}`,
  `${candidate.name.first}-${candidate.name.last}`
]);

module.exports = pipe(makeContribsUrl, encaseP(mainGetter));