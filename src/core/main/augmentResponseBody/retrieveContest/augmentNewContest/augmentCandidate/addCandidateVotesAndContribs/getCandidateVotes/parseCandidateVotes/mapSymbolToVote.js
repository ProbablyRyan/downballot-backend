'use strict';

module.exports = symbol => ({
    '-': 'Abstained',
    'C': 'Co-sponsor',
    'N': 'No',
    'P': 'Signed',
    'S': 'Sponsor',
    'V': 'Vetoed',
    'Y': 'Yes'
})[symbol] || String(symbol);