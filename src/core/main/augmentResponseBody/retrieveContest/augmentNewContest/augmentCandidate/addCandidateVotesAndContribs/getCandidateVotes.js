'use strict';

const { map } = require('fluture');
const { ifElse } = require('ramda');
const parseCandidateVotes = require('./getCandidateVotes/parseCandidateVotes');
const queryCandidateVotes = require('./getCandidateVotes/queryCandidateVotes');

const handleVotesResponse = ifElse(
  res => res?.status === 200 && !res.data?.error,
  parseCandidateVotes,
  () => ({})
);

module.exports = function getCandidateVotes(candidate) {
  return queryCandidateVotes(candidate)
    .pipe(map(handleVotesResponse));
};
