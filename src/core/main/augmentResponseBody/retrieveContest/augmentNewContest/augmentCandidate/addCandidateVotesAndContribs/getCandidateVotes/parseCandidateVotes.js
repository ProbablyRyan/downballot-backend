'use strict';

const { decode } = require('he');
const mapSymbolToVote = require('./parseCandidateVotes/mapSymbolToVote');

module.exports = function parseCandidateVotes(res) {
  return {
    votes: {
      list: res
        .data
        .bills
        .bill
        // Get 5 most recent votes
        .slice(0, 5)
        // Return bill name and candidate vote for each bill
        .map(bill =>
          ({
            // Remove HTML escape characters from bill title
            title: decode(bill.title),
            // Replace VoteSmart vote symbol with full string
            vote: mapSymbolToVote(bill.vote)
          })
        )
    }
  };
};