'use strict';

const { map } = require('fluture');
const { ifElse } = require('ramda');
const requestFinancePage = require('./scrapeCandidateContribs/requestFinancePage');
const parseFinancePage = require('./scrapeCandidateContribs/parseFinancePage');

const handleFinanceResponse = ifElse(
  res => res?.status === 200,
  parseFinancePage,
  () => ({})
);

module.exports = function scrapeCandidateContribs(candidate) {
  return requestFinancePage(candidate)
    .pipe(map(handleFinanceResponse));
};
