'use strict';

const { encaseP } = require('fluture');
const { pipe } = require('ramda');
const mainGetter = require('../../../../../../../utils/asyncIO/mainGetter');
const urlMaker = require('../../../../../../../utils/urlMaker');

const makeVotesApiUrl = candidate => urlMaker('http://api.votesmart.org/Votes.getByOfficial?')({
  key: process.env.VOTESMART_API_KEY,
  candidateId: candidate.voteSmartID,
  o: 'JSON'
});

module.exports = pipe(makeVotesApiUrl, encaseP(mainGetter));
