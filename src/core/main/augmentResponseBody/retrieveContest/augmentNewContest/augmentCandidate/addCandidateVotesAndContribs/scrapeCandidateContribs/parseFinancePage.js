'use strict';

const cheerio = require('cheerio');
const { decode } = require('he');

module.exports = function parseFinancePage(res) {
  const $ = cheerio.load(res.data);

  return $('#crpContributors').length === 1
    ? {
      contributors: {
        // Find "Top Contributors" table by element id
        list: $('#crpContributors')
          .find('tbody')
          .find('tr')
          .map((i, el) =>
            $(el).children()
          )
          .get()
          // Get 5 highest contributors
          .slice(0, 5)
          .map(contrib => ({
            // Remove HTML escape characters from contributor name
            name: decode($(contrib['0']).text()),
            amount: $(contrib['1']).text()
          }))
      }
    }
    : {};
};