'use strict';

const { chain, map, resolve, reject } = require('fluture');
const { set } = require('ramda');
const makeMatchParser = require('./getVoteSmartCandidateID/makeMatchParser');
const requestVoteSmartID = require('./getVoteSmartCandidateID/requestVoteSmartID');
const { voteSmartID } = require('../../../../../helpers/lenses');

module.exports = function getVoteSmartCandidateID(candidate) {
  const matchParser = makeMatchParser(candidate);

  return requestVoteSmartID(candidate)
    .pipe(map(matchParser))
    .pipe(chain(match => match?.candidateId
      ? resolve(set(voteSmartID, match.candidateId, candidate))
      : reject()
    ));
};
