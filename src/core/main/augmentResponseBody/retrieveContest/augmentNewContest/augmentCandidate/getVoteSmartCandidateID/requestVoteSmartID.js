'use strict';

const { encaseP } = require('fluture');
const { pipe } = require('ramda');
const mainGetter = require('../../../../../../utils/asyncIO/mainGetter');
const urlMaker = require('../../../../../../utils/urlMaker');

const makeContribsUrl = candidate => urlMaker('http://api.votesmart.org/Candidates.getByLastname?')({
        key: process.env.VOTESMART_API_KEY,
        lastName: candidate.name.last,
        o: 'JSON'
      });

module.exports = pipe(makeContribsUrl, encaseP(mainGetter));
