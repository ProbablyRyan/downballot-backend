'use strict';

const statusIsNot200 = res => res?.status !== 200;
const hasNoCandidateList = res => !res?.data?.candidateList;
const candidateListIsNotArray = list => Array.isArray(list) === false;

module.exports = function makeMatchParser(target) {
  return function parseResponse(res) {
    return statusIsNot200(res) || hasNoCandidateList(res)
        ? {}
        : parseSomeMatches(res.data.candidateList.candidate);
  };

  function parseSomeMatches(candidateList) {
    return candidateListIsNotArray(candidateList)
      ? candidateList
      : parseMultipleMatches(candidateList);
  };

  function parseMultipleMatches(list) {
    const matches = list.filter(match =>
      (target.name.first === match.firstName || target.name.first === match.nickName)
    );

    if (matches.length === 1) {
      return matches[0];
    } else if (matches.length > 1) {
      return parseDuplicateMatches(matches);
    } else {
      return {};
    }
  };

  function parseDuplicateMatches(matches) {
    let allAreDuplicates = true;
  
    for (let i = 1; i < matches.length; i++) {
      allAreDuplicates = allAreDuplicates && matches[i].candidateId === matches[i-1].candidateId;
  
      if (!allAreDuplicates) {
        return {};
      }
    }
  
    return matches[0];
  };
};
