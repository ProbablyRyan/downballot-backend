'use strict';

const { clean } = require('diacritic');
const { set } = require('ramda');
const { name } = require('../../../../helpers/lenses');

module.exports = function splitName(candidate) {
  // Parse primary candidate from split ticket (e.g. "Joe Biden/Kamala Harris" becomes "Joe Biden")
  const normalizedName = /[\w\s\."-]+/
    .exec(
      // Normalize diacritics
      clean(candidate.name)
    )[0];

  const nameArray = normalizedName
    .split(' ')
    .filter(fragment =>
      // Avoid setting generational suffixes as last name
      !/^[ivx]+$|Sr\.|Jr\.|Esq\./i.test(fragment)
    );

  return set(
    name,
    {
      full: normalizedName,
      first: nameArray[0],
      last: nameArray[nameArray.length - 1]
    },
    candidate
  );
};