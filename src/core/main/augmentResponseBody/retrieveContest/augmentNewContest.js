'use strict';

const { parallel, map } = require('fluture');
const { set, compose } = require('ramda');
const { candidates } = require('../../../helpers/lenses');
const augmentCandidate = require('./augmentNewContest/augmentCandidate');
const splitName = require('./augmentNewContest/splitName');

module.exports = function augmentNewContest(contest) {
  return parallel
    (contest.candidates.length)
    (contest.candidates.map(compose(augmentCandidate, splitName)))
    .pipe(map(res =>
      set(candidates, res, contest)
    ));
};