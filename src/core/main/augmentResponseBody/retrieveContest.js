'use strict';

const { alt } = require('fluture');
const augmentNewContest = require('./retrieveContest/augmentNewContest');
const queryCachedContest = require('./retrieveContest/queryCachedContest');

module.exports = function retrieveContest(contest) {
    return alt(augmentNewContest(contest))(queryCachedContest(contest));
};