'use strict';

const initResponseFromQuery = require('./initResponseFromAddress/initResponseFromQuery');
const prepResponseBody = require('./initResponseFromAddress/prepResponseBody');
const { map } = require('fluture');
const makeVoterInfoQuery = require('./initResponseFromAddress/makeVoterInfoQuery');

module.exports = function initResponseFromAddress(address) {
  return makeVoterInfoQuery(address)
    .pipe(map(initResponseFromQuery))
    .pipe(map(prepResponseBody));
};


