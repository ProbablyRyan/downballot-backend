'use strict';

const { reduce, pipe } = require('lodash/fp');
const { over } = require('ramda');
const { filterBallot, filterContest } = require('../../helpers/filters');
const { contests } = require('../../helpers/lenses');

const classifyContests = contest =>
  contest?.type === 'Referendum' ? 'referenda' 
  : contest?.candidates ? 'augmented' 
  : 'basic';

const contestReducer = (accumulator, currentValue) => {
  const valueType = classifyContests(currentValue);
  accumulator[valueType].push(filterContest[valueType](currentValue));
  return accumulator;
};

const reduceContests = contests => {
  let accumulator = {
    augmented: [],
    basic: [],
    referenda: []
  };

  return reduce(contestReducer)(accumulator)(contests);
};

module.exports = function prepResponseBody(res) {
  const ballot = pipe(filterBallot, over(contests, reduceContests))(res.body);
  return res.setBody(ballot);
};