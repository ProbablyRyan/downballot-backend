'use strict';

const { encaseP } = require('fluture');
const { pipe } = require('ramda');
const mainGetter = require('../../utils/asyncIO/mainGetter');
const urlMaker = require('../../utils/urlMaker');

const makeVoterInfoUrl = address => urlMaker('https://www.googleapis.com/civicinfo/v2/voterinfo?')({
  key: process.env.GOOGLE_API_KEY,
  address: encodeURIComponent(address)
});

module.exports = pipe(makeVoterInfoUrl, encaseP(mainGetter));