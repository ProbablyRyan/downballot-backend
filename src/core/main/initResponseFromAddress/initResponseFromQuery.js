'use strict';

const Response = require('../../utils/Response');

module.exports = function initResponseFromQuery(queryData) {
  return queryData?.status === 200 && queryData?.data?.contests
    ? Response({
      statusCode: 200,
      body: queryData.data
    })
    : Response({ statusCode: 404 });
};