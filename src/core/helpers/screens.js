'use strict';

module.exports = {
  augmentedScreen: [
    'candidates',
    'district',
    'office'
  ],
  ballotScreen: [
    'contests',
    'election'
  ],
  basicScreen: [
    'district',
    'office'
  ],
  candidateScreen: [
    'candidateUrl',
    'channels',
    'contributors',
    'email',
    'name',
    'party',
    'phone',
    'votes'
  ],
  referendaScreen: [
    'referendumSubtitle', 
    'referendumTitle', 
    'referendumUrl'
  ]  
};
