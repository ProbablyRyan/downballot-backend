'use strict';

const { pipe } = require('lodash/fp');
const { lensPath, lensProp } = require('ramda');

const contests = lensProp('contests');
const augmentedContests = lensPath(['contests', 'augmented']);
const candidates = lensProp('candidates');
const contribsSource = lensPath(['contributors', 'source']);
const name = lensProp('name');
const state = lensProp('state');
const voteSmartID = lensProp('voteSmartID');
const votesSource = lensPath(['votes', 'source']);

module.exports = {
  augmentedContests,
  candidates,
  contests,
  contribsSource,
  name,
  state,
  voteSmartID,
  votesSource
};
