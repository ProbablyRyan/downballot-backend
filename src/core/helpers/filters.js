'use strict';

const {
  over,
  pickAll,
  compose
} = require('ramda');
const { candidates, state } = require('./lenses');
const {
  augmentedScreen,
  ballotScreen,
  basicScreen,
  candidateScreen,
  referendaScreen
} = require('./screens');

const filterBallot = pickAll(ballotScreen);

const filterContest = {
  augmented: compose(pickAll(augmentedScreen), over(candidates, focus => focus.map(pickAll(candidateScreen)))),
  basic: pickAll(basicScreen),
  referenda: pickAll(referendaScreen)
};

module.exports = {
  filterBallot,
  filterContest
};
