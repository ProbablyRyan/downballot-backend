'use strict';

const { model, Schema } = require('mongoose');

const CandidateSchema = new Schema({
  candidateUrl: String,
  channels: [Object],
  contributors: {
    list: [{
      name: String,
      amount: String
    }],
    source: String
  },
  email: String,
  name: {
    type: Object,
    required: true
  },
  party: String,
  phone: String,
  votes: {
    list: [{
      title: String,
      vote: String
    }],
    source: String
  },
  voteSmartID: String
}, {
  _id: false
});

const ContestSchema = new Schema({
  ballotTitle: String,
  candidates: {
    type: [CandidateSchema],
    required: true
  },
  district: {
    name: {
      type: String,
      required: true
    },
    scope: String,
    id: String
  },
  expireAt: {
    type: Date,
    required: true
  },
  office: {
    type: String,
    required: true
  },
  numberVotingFor: Number,
  primaryParty: String,
  type: String
}, {
  bufferCommands: false
});

ContestSchema.index({ expireAt: 1 }, { expireAfterSeconds: 0 });

const Contest = model('contest', ContestSchema);

module.exports = Contest;
