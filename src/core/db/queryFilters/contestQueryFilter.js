'use strict';

module.exports = ({ district, office }) => ({
  district,
  office
});