'use strict';

const { identity } = require('lodash');

module.exports = function doAsyncIO(kept = identity, broken = identity) {
  return (asyncFn) => (...args) => asyncFn(...args).then(kept, broken);
};