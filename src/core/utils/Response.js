'use strict';

module.exports = function Response({ statusCode, body = {} }) {
  const prototype = {
    bodyAsJSON: function() {
      return {
        ...this,
        body: JSON.stringify(this.body)
      };
    },

    getBody: function() {
      return {
        ...this.body
      };
    },

    setBody: function(newBody) {
      if (this.statusCode >= 200 && this.statusCode <= 299) {
        this.body = newBody;
      }
      return this;
    }
  };

  return Object.assign(
    Object.create(prototype),
    {
      headers: {
        "Access-Control-Allow-Origin" : process.env.CORS_ORIGIN
      }
    },
    {
      statusCode,
      body
    }
  );
};
