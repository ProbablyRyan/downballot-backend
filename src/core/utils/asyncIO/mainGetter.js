'use strict';

const axios = require('axios');
const doAsyncIO = require('../doAsyncIO');
const axiosConfig = require('../../../../config/axios');

module.exports = doAsyncIO()(url => axios({
  method: 'GET',
  url,
  ...axiosConfig
}));
