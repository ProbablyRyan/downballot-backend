'use strict';

const { pipe, stubTrue } = require('lodash/fp');
const {
  cond,
  identity,
  set
} = require('ramda');
const {
  contribsSource,
  votesSource
} = require('../helpers/lenses');
const urlMaker = require('./urlMaker');

module.exports = conditional => lens => value => cond([
  [conditional, set(lens, value)],
  [stubTrue, identity]
]);

// const makeContribsUrl = urlMaker('https://justfacts.votesmart.org/candidate/campaign-finance');
// const makeVotesSourceUrl = urlMaker('https://justfacts.votesmart.org/candidate/key-votes');

// const hasContribsProp = obj => obj.hasOwnProperty('contributors');
// const hasVotesProp = obj => obj.hasOwnProperty('votes');

// const condSetContribsSource = condSet(hasContribsProp)(contribsSource);
// const condSetVotesSource = condSet(hasVotesProp)(votesSource);

// module.exports = {
//   condSetContribsUrl: pipe(makeContribsUrl, condSetContribsSource),
//   condSetVotesUrl: pipe(makeVotesSourceUrl, condSetVotesSource)
// };