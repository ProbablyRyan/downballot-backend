'use strict';

const { reduce } = require('lodash');

module.exports = (base = '') => (parameters = []) => reduce(
  parameters, 
  (string, value, key) =>
    Array.isArray(parameters) ? string + `/${value}` : string + `${key}=${value}&`,
  base
);
